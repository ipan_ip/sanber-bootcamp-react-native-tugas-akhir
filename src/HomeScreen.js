import React from 'react';
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, Dimensions, TextInput, Button } from 'react-native';

import data from './data.json';
import axios from 'axios';

const DEVICE = Dimensions.get('window')

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      searchText: '',
      isLoaded: false,
      lembagas: []
    }
  }

  componentDidMount() {
    axios.get('http://si-praktikum-backend.herokuapp.com/api/v1/lembaga/')
      .then((res) => {
        this.setState({
          lembagas: res.data.reverse(),
        })
      })
  }

  render() {
    console.log(this.state.lembagas)
    console.log(data.produk)
    const userName = this.props.route.params.userName
    // const userName = ''
    return (
      <View style={styles.container}>
        <View style={{ minHeight: 50, width: DEVICE.width * 0.88 + 20, marginVertical: 8 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text>Hai,{'\n'}
              <Text style={styles.headerText}>{userName}</Text>
            </Text>
          </View>
          <View>

          </View>
          <TextInput
            style={{ backgroundColor: 'white', marginTop: 8 }}
            placeholder='Cari lembaga..'
            onChangeText={(searchText => this.setState({ searchText }))}
          />
        </View>
        <FlatList 
          data={this.state.lembagas}
          renderItem={(lembaga) =>
            <Lembaga data={lembaga.item}/>
          }
          keyExtractor={(item) => item.id}
          ItemSeparatorComponent={() => <View style={{
            marginVertical: 10
            // height: 5,
            // backgroundColor: 'black'
          }}></View>}
        />
      </View>
    )
  }
};

class Lembaga extends React.Component {
  constructor(props) {
    super(props)
  }
  render() {
    const data = this.props.data
    console.log(data)
    return (
      <View style={styles.itemContainer}>
        <Image source={{ uri: data.gambar }} style={styles.itemImage} resizeMode='contain' />
        <View style={{textAlign: 'center'}}>
          <Text numberOfLines={2} ellipsizeMode='tail' style={styles.itemName, {fontWeight: 'bold'}} >{data.nama}</Text>
          <Text numberOfLines={2} ellipsizeMode='tail' style={styles.itemName} >{data.tema.nama}</Text>
          <Text numberOfLines={2} ellipsizeMode='tail' style={styles.itemName} >{data.deskripsi_singkat}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold'
  },  
  itemContainer: {
    width: DEVICE.width * 0.44,
  },
  itemImage: {
    height: 100
  }
})
